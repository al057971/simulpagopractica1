package simulpago;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
public class Frame extends javax.swing.JFrame {     //botonIniciar desencadena todo
    String cola[] = new String[16];                 //Almacena las posiciones de los alumnos
    int espera[] = new int[16];                     //Almacena los tiempos de espera individual
    String[] arreglo={"Aceptar"};
    int colaLengt;                                  //Tamaño de la cola
    int menorTiempo;                                //va a labelMinEstimado
    int mayorTiempo;                                //va a labelMaxEstimado
    int posMenor;
    int posMayor;
    int tiempoTotal = 0;                            //va a labelTiempoReal
    int contadorSalieron=0;                         //Cuenta cuantos alumnos salen de la cola
    int contadorPermanecen=0;                       //Cuenta cuantos alumnos permanecen en el sistema
//----numero aleatorio emtre 10 y 15--------------------------------------------
    public int numeroRandom(){
        int aleatorio= (int) (Math.random() *10)+1;
        return aleatorio;
    }
//----INICIALIZA LA COLA--------------------------------------------------------
    public void inicio(){                                                       //se usa la nomenclatura "posN" para los label, donde N es el numero de posicion
        for (int i = 1; i < 16; i++) {                                          //llena todas las posiciones con " " para evitar tener valores null
            cola[i]=" ";
        }
        int x = numeroRandom();                                                 //determina el numero de elementos que tendrá la cola
        colaLengt=x;                                                            //Tamaño de la cola como variable de clase    
              
        for (int i=1;i <=x;i++) {                                               //ciclo que se ejecuta hasta alcanzar el número aleatorio x
            cola[i]= String.valueOf(i);
            espera[i]=numeroRandom();
            
            if (i!=1) {
                tiempoTotal=tiempoTotal+espera[i];
            } else {
                tiempoTotal=espera[1];
            }
            System.out.println(espera[i]);
            labelTiempoReal.setText(String.valueOf(tiempoTotal));
            
            switch(i){
            case 1:
                pos1.setText(String.valueOf(i));
            break;
            case 2:
                pos2.setText(String.valueOf(i));
            break;
            case 3:
                pos3.setText(String.valueOf(i));
            break;
            case 4:
                pos4.setText(String.valueOf(i));
            break;
            case 5:
                pos5.setText(String.valueOf(i));
            break;
            case 6:
                pos6.setText(String.valueOf(i));
            break;
            case 7:
                pos7.setText(String.valueOf(i));
            break;
            case 8:
                pos8.setText(String.valueOf(i));
            break;
            case 9:
                pos9.setText(String.valueOf(i));
            break;
            case 10:
                pos10.setText(String.valueOf(i));
            break;
            case 11:
                pos11.setText(String.valueOf(i));
            break;
            case 12:
                pos12.setText(String.valueOf(i));
            break;
            case 13:
                pos13.setText(String.valueOf(i));
            break;
            case 14:
                pos14.setText(String.valueOf(i));
            break;
            case 15:
                pos15.setText(String.valueOf(i));
            break;
            case 16://*** EN CASO DE QUE SE ALCANCE EL VALOR x POR F
            break;
            default:
            break;
            }
        }  
    }
//----HACE AVANZAR LA COLA------------------------------------------------------
    public void avanzar(){
        salir();
        int x=1;
        menorTiempo=espera[1];
        mayorTiempo=espera[1];
        for (int i = 1; i <= colaLengt; i++) {
            if (espera[i]<menorTiempo) {
                menorTiempo=espera[i];
                posMenor=i;
            } else {
                if (espera[i]>mayorTiempo) {
                    mayorTiempo=espera[i];
                    posMayor=i;
                }
            }
            if(posMenor==0){
                posMayor=1;
            }
            if(posMenor==0){
                posMenor=1;
            }
        }
        do{
           cola[1]=cola[2];
           cola[2]=cola[3];
           cola[3]=cola[4];
           cola[4]=cola[5];
           cola[5]=cola[6];
           cola[6]=cola[7];
           cola[7]=cola[8];
           cola[8]=cola[9];
           cola[9]=cola[10];
           cola[10]=cola[11];
           cola[11]=cola[12];
           cola[12]=cola[13];
           cola[13]=cola[14];
           cola[14]=cola[15];
           cola[15]=" ";
           try{
               Thread.sleep(espera[x]*500);
           }catch(InterruptedException ex){   
           }
            System.out.println("espera: "+espera[x]);
            labelPorcentajeSalieron.setText(String.valueOf(espera[x]));
            pos1.setText(String.valueOf(cola[1]));
            pos2.setText(String.valueOf(cola[2]));
            pos3.setText(String.valueOf(cola[3]));
            pos4.setText(String.valueOf(cola[4]));
            pos5.setText(String.valueOf(cola[5]));
            pos6.setText(String.valueOf(cola[6]));
            pos7.setText(String.valueOf(cola[7]));
            pos8.setText(String.valueOf(cola[8]));
            pos9.setText(String.valueOf(cola[9]));
            pos10.setText(String.valueOf(cola[10]));
            pos11.setText(String.valueOf(cola[11]));
            pos12.setText(String.valueOf(cola[12]));
            pos13.setText(String.valueOf(cola[13]));
            pos14.setText(String.valueOf(cola[14]));
            pos15.setText(String.valueOf(cola[15]));
            this.paintAll(this.getGraphics());                                   //Refresca la pantalla
            x++;
        }while (x<=colaLengt);
            labelMaxEstimado.setText(String.valueOf(posMenor));
            labelMinEstimado.setText(String.valueOf(posMayor));
            System.out.println(menorTiempo+" menor "+posMenor);
            System.out.println(mayorTiempo+" mayor "+posMayor);
            labelPorcentajeSalieron.setText("0");
            JOptionPane.showOptionDialog(null, "Cola vacía", " ", 0, JOptionPane.QUESTION_MESSAGE, null, arreglo, "Aceptar");
            this.dispose();
    }
//----alumnos que se salen------------------------------------------------------
    public void salir(){
        if (tiempoTotal > 50) {
            JOptionPane.showOptionDialog(null, "Un alumno salió de la fila", " ", 0, JOptionPane.QUESTION_MESSAGE, null, arreglo, "Aceptar");
            cola[colaLengt]=" ";
            }
    }
//------------------------------------------------------------------------------    
    public Frame() {
        initComponents();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        BotonIniciar = new javax.swing.JButton();
        pos1 = new javax.swing.JLabel();
        pos2 = new javax.swing.JLabel();
        pos3 = new javax.swing.JLabel();
        pos4 = new javax.swing.JLabel();
        pos5 = new javax.swing.JLabel();
        pos6 = new javax.swing.JLabel();
        pos7 = new javax.swing.JLabel();
        pos8 = new javax.swing.JLabel();
        pos9 = new javax.swing.JLabel();
        pos10 = new javax.swing.JLabel();
        pos11 = new javax.swing.JLabel();
        pos12 = new javax.swing.JLabel();
        pos13 = new javax.swing.JLabel();
        pos14 = new javax.swing.JLabel();
        pos15 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        labelTiempoReal = new javax.swing.JLabel();
        labelPorcentajeSalieron = new javax.swing.JLabel();
        labelMaxEstimado = new javax.swing.JLabel();
        labelMinEstimado = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        jLabel4.setText("jLabel4");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        BotonIniciar.setBackground(new java.awt.Color(0, 102, 102));
        BotonIniciar.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 14)); // NOI18N
        BotonIniciar.setForeground(new java.awt.Color(255, 255, 255));
        BotonIniciar.setText("INICIAR");
        BotonIniciar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.gray, java.awt.Color.lightGray, java.awt.Color.black, java.awt.Color.darkGray));
        BotonIniciar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                BotonIniciarMouseClicked(evt);
            }
        });

        pos1.setFont(new java.awt.Font("Calibri Light", 1, 18)); // NOI18N
        pos1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 3, true));

        pos2.setFont(new java.awt.Font("Calibri Light", 1, 18)); // NOI18N
        pos2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 3, true));

        pos3.setFont(new java.awt.Font("Calibri Light", 1, 18)); // NOI18N
        pos3.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 3, true));

        pos4.setFont(new java.awt.Font("Calibri Light", 1, 18)); // NOI18N
        pos4.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 3, true));

        pos5.setFont(new java.awt.Font("Calibri Light", 1, 18)); // NOI18N
        pos5.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 3, true));

        pos6.setFont(new java.awt.Font("Calibri Light", 1, 18)); // NOI18N
        pos6.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 3, true));

        pos7.setFont(new java.awt.Font("Calibri Light", 1, 18)); // NOI18N
        pos7.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 3, true));

        pos8.setFont(new java.awt.Font("Calibri Light", 1, 18)); // NOI18N
        pos8.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 3, true));

        pos9.setFont(new java.awt.Font("Calibri Light", 1, 18)); // NOI18N
        pos9.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 3, true));

        pos10.setFont(new java.awt.Font("Calibri Light", 1, 18)); // NOI18N
        pos10.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 3, true));

        pos11.setFont(new java.awt.Font("Calibri Light", 1, 18)); // NOI18N
        pos11.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 3, true));

        pos12.setFont(new java.awt.Font("Calibri Light", 1, 18)); // NOI18N
        pos12.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 3, true));

        pos13.setFont(new java.awt.Font("Calibri Light", 1, 18)); // NOI18N
        pos13.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 3, true));

        pos14.setFont(new java.awt.Font("Calibri Light", 1, 18)); // NOI18N
        pos14.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 3, true));

        pos15.setFont(new java.awt.Font("Calibri Light", 1, 18)); // NOI18N
        pos15.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 3, true));

        jButton2.setBackground(new java.awt.Color(51, 51, 51));
        jButton2.setFont(new java.awt.Font("Bauhaus 93", 1, 18)); // NOI18N
        jButton2.setForeground(new java.awt.Color(255, 255, 255));
        jButton2.setText("CAJA");

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Alumno que esperó menos");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Alumno que esperó más");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Tiempo de ejecucion: ");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Tiempo de servicio actual: ");

        labelTiempoReal.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        labelTiempoReal.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED, java.awt.Color.darkGray, null, java.awt.Color.lightGray, java.awt.Color.gray));

        labelPorcentajeSalieron.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        labelPorcentajeSalieron.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED, java.awt.Color.darkGray, null, java.awt.Color.lightGray, java.awt.Color.gray));

        labelMaxEstimado.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        labelMaxEstimado.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED, java.awt.Color.darkGray, null, java.awt.Color.lightGray, java.awt.Color.gray));

        labelMinEstimado.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        labelMinEstimado.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED, java.awt.Color.darkGray, null, java.awt.Color.lightGray, java.awt.Color.gray));

        jButton1.setBackground(new java.awt.Color(0, 102, 102));
        jButton1.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 14)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("ABRIR CAJA");
        jButton1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.gray, java.awt.Color.lightGray, java.awt.Color.black, java.awt.Color.darkGray));
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton1MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(BotonIniciar, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(labelMaxEstimado, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(pos15, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(pos14, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(pos13, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(pos12, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(pos11, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(pos10, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(pos9, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(pos8, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(44, 44, 44)
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(labelMinEstimado, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(pos7, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(pos6, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(pos5, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pos4, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(pos3, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(pos2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(68, 68, 68)
                                .addComponent(jLabel3)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelPorcentajeSalieron, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelTiempoReal, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(100, 100, 100)))
                .addComponent(pos1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel1)
                                        .addComponent(labelMaxEstimado, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(BotonIniciar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel2)
                                            .addComponent(labelMinEstimado, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(31, 31, 31))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel3)
                                    .addComponent(labelTiempoReal, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel5)
                                    .addComponent(labelPorcentajeSalieron, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(pos1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pos2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pos3, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pos4, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pos5, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pos6, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pos7, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pos8, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pos9, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pos10, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pos11, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pos12, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pos13, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pos14, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pos15, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(50, 50, 50)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BotonIniciarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonIniciarMouseClicked
        inicio(); 
    }//GEN-LAST:event_BotonIniciarMouseClicked

    private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseClicked
       avanzar();
    }//GEN-LAST:event_jButton1MouseClicked

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Frame().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BotonIniciar;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel labelMaxEstimado;
    private javax.swing.JLabel labelMinEstimado;
    private javax.swing.JLabel labelPorcentajeSalieron;
    private javax.swing.JLabel labelTiempoReal;
    private javax.swing.JLabel pos1;
    private javax.swing.JLabel pos10;
    private javax.swing.JLabel pos11;
    private javax.swing.JLabel pos12;
    private javax.swing.JLabel pos13;
    private javax.swing.JLabel pos14;
    private javax.swing.JLabel pos15;
    private javax.swing.JLabel pos2;
    private javax.swing.JLabel pos3;
    private javax.swing.JLabel pos4;
    private javax.swing.JLabel pos5;
    private javax.swing.JLabel pos6;
    private javax.swing.JLabel pos7;
    private javax.swing.JLabel pos8;
    private javax.swing.JLabel pos9;
    // End of variables declaration//GEN-END:variables
}
